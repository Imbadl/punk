# slic: duplicate license

## Purpose

DEPRECATED: all licenses are now migrated from LicenseDot to CustomersDot, so there's no need for manual copy/pasting anymore.

Allows to duplicate licenses from LicenseDot to CustomersDot. \
This script will be rendered useless once all licenses currently in LicenseDot are imported into CustomersDot (see [EOL - LicenseDot](https://gitlab.com/groups/gitlab-org/-/epics/6327)).

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/slic_duplicate/script.user.js

## Use

License duplication is a two step manual process:
- Go to the license page in LicenseDot to copy information
- Go to the `Add new Offline License` page in CustomersDot to paste the information

Double check the pasted information!

Hotkeys: 
- `ctrl-l ctrl-c` to copy license info in LicenseDot
- `ctrl-l ctrl-v` to paste license info in CustomersDot

Alternatively use the `Duplicate license - copy` and `Duplicate license paste commands from the userscript manager extension dropdown. 

## Configuration

None.

## Changelog

- 1.0.3
  - Don't paste 'N/A' into Zuora subscription fields
- 1.0.2
  - Better URL matching
- 1.0.1
  - Name fix
- 1.0.0
  - Initial release
