// ==UserScript==
// @name          slic duplicate
// @version       1.0.3
// @author        Rene Verschoor
// @description   slic duplicate license from LicenseDot to CustomersDot
// @match         https://license.gitlab.com/*
// @match         https://customers.gitlab.com/admin/*
// @grant         GM_registerMenuCommand
// @grant         GM_setValue
// @grant         GM_getValue
// @grant         GM_deleteValue
// @require       https://cdn.jsdelivr.net/combine/npm/@violentmonkey/dom@1,npm/@violentmonkey/ui@0.6
// @require       https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/slic_duplicate
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/slic_duplicate/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/slic_duplicate/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/slic_duplicate/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

// https://github.com/violentmonkey/vm-shortcut
const HOTKEY_COPY = 'ctrl-l ctrl-c';
const HOTKEY_PASTE = 'ctrl-l ctrl-v';

(function () {

  function toast(message) {
    VM.showToast(message);
  }

  function registerHotkey() {
    VM.shortcut.register(HOTKEY_COPY, () => {
      duplicateLicenseCopy();
    });
    VM.shortcut.register(HOTKEY_PASTE, () => {
      duplicateLicensePaste();
    });
  }

  function registerMenu() {
    GM_registerMenuCommand('Duplicate license - copy', duplicateLicenseCopy);
    GM_registerMenuCommand('Duplicate license - paste', duplicateLicensePaste);
  }

  function scrapeLicenseDot(url) {
    let info = {};
    info.url = url;
    // Two columns contain dl's
    let dt = document.querySelectorAll('div.container-fluid.mt-3 div.row.mt-3 div.col-md-6 dl dt');
    let dd = document.querySelectorAll('div.container-fluid.mt-3 div.row.mt-3 div.col-md-6 dl dd');
    if (dt.length && (dt.length == dd.length)) {
      for (let i = 0; i < dt.length; i++) {
        // Field name, eg 'First name' -> remove LF/CR, trailing ':', lowercase, snake_case -> 'first_name'
        let key = dt[i].textContent.trim().toLowerCase().replace(/ /g, '_').replace(/:$/, '');
        // Field value, eg 'Jabberwocky' -> remove LF/CR
        let value = dd[i].textContent.trim();
        info[key] = value;
      }
    } else {
      toast('ERROR: dt and dd length not equal');
    }
  return info;
  }

  async function duplicateLicenseCopy() {
    await GM_deleteValue('DuplicateLicenseInfo');
    let info = {};
    let url = document.URL;
    if (url.match(/^https:\/\/license.gitlab.com\/licenses\/\d+/)) {
      info = scrapeLicenseDot(url);
      await GM_setValue('DuplicateLicenseInfo', JSON.stringify(info));
      toast('License copied for duplication');
    } else {
      toast('ERROR: can only duplicate from LicenseDot');
    }
  }

  async function duplicateLicensePaste() {
    let url = document.URL;
    if (url.match(/^https:\/\/customers.gitlab.com\/admin\/license\/new_license/)) {
      let info = JSON.parse(await GM_getValue('DuplicateLicenseInfo', '{}'));
      duplicateLicenseFill(info);
      await GM_deleteValue('DuplicateLicenseInfo');
    } else {
      toast('ERROR: can only duplicate to CustomersDot > Licenses > New Offline License');
    }
  }

  function duplicateLicenseFill(info) {
    setField('license_name', info.name);
    setField('license_company', info.company);
    setField('license_email', info.email);
    setField('license_zuora_subscription_id', skipNA(info.zuora_subscription));
    setField('license_zuora_subscription_name', skipNA(info.zuora_subscription_name));
    setField('license_users_count', info.users_count.replace(/,/g, ''));
    setField('license_previous_users_count', info.previous_users_count);
    setField('license_trueup_quantity', info.trueup_count);
    setDropdown('license_plan_code', info.gitlab_plan);
    setCheckbox('license_trial', info.trial);
    setField('license_starts_at', dateFuckery(info.starts_at));
    setField('license_expires_at', dateFuckery(info.expires_at));
    setField('license_notes', info.notes);
  }

  function setField(field, value) {
    document.getElementById(field).value = value;
  }

  function setDropdown(field, plan) {
    document.querySelector('[data-input-for="license_plan_code"] .input-group-btn').click();
    [...document.querySelector('ul#ui-id-1').children].filter(option => option.textContent === plan)[0].click();
  }

  function setCheckbox(field, trial) {
    let value = trial === 'Yes' ? true : false;
    let checkbox = document.getElementById(field);
    if (checkbox.checked != value) {
      checkbox.click();
    }
  }

  // Datepicker is 'MMMM DD, YYYY' instead of ISO 8601
  // Convert e.g. '2021-12-31' to 'December 12, 2021'
  function dateFuckery(yyyymmdd) {
    let date = new Date(yyyymmdd + 'Z');
    let day = date.getUTCDate();
    day = day < 10 ? '0' + day : day;
    let month = date.toLocaleString('en-US', { month: 'long', timeZone: 'UTC' });
    let year = date.getFullYear();
    let longdate = `${month} ${day}, ${year}`;
    return longdate;
  }

  function skipNA(value) {
    if (value == 'N/A') {
      value = ''
    }
    return value;
  }

  function slic_duplicate() {
    registerMenu();
    registerHotkey();
  }

  slic_duplicate();

})();
