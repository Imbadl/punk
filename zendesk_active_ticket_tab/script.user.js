// ==UserScript==
// @name          Zendesk active ticket tab
// @version       1.0.2
// @author        Rene Verschoor
// @description   Zendesk: highlight active ticket tab
// @match         https://gitlab.zendesk.com/*
// @grant         GM_getValue
// @grant         GM_setValue
// @grant         GM_addStyle
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_active_ticket_tab
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_active_ticket_tab/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_active_ticket_tab/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_active_ticket_tab/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const DEFAULT_COLOR = 'SkyBlue';

(async function() {
  // Read user configuration from userscript manager's Values settings
  const value = await GM_getValue('color');
  if (value === undefined) {
    // No color is configured, store default color in Values settings
    await GM_setValue('color', DEFAULT_COLOR);
  }
  let tabColor = value || DEFAULT_COLOR;
  // Once we have a color, paint the active ticket tab
  let elem = document.querySelector('div[role=tab][data-selected=true]');
  GM_addStyle(`div[role=tab][data-selected=true] { background: ${tabColor}; }`);
})();

(function() {
})();