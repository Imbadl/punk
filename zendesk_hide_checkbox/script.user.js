// ==UserScript==
// @name          Zendesk hide checkbox
// @version       1.0
// @author        Rene Verschoor
// @description   Zendesk: hide checkbox column in views
// @match         https://gitlab.zendesk.com/*
// @grant         GM_addStyle
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_checkbox
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_checkbox/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_checkbox/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_checkbox/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {
  GM_addStyle('[data-test-id="generic-table-head"] > tr > th:nth-child(1) { display: none }');
  GM_addStyle('[data-test-id="generic-table-head"] > tr > th:nth-child(3) { display: none }');
  GM_addStyle('[data-test-id="generic-table-row"] > td:nth-child(1) { display: none }');
  GM_addStyle('[data-test-id="generic-table-row"] > td:nth-child(3) { display: none }');
})();
