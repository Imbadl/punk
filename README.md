# Userscript repository

#### Be your own workspace anarchist!

## How to use

These scripts are meant to be used with a userscript manager like [Violentmonkey](https://violentmonkey.github.io).

## Compatibility

Violentmonkey is available for Chrome, Firefox, Edge, Chromium, Opera, Vivaldi, etc. \
The userscripts might be compatible with other userscript managers like [Tampermonkey](https://www.tampermonkey.net).

I've only used these scripts with [Violentmonkey](https://violentmonkey.github.io) and [Vivaldi](https://vivaldi.com), so you might run into incompatibility issues (yay browser API compatibility!).

## Scripts

Zendesk:
- [Zendesk: hide signature](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_signature)
- [Zendesk: active ticket tab](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_active_ticket_tab)
- [Zendesk: SLA colors](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_sla_colors)
- [Zendesk: trim user info](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_user_info) (Katrin Leinweber)
- [Zendesk: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hayfever) (broken)
- [Zendesk: short SLA](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_short_sla)
- [Zendesk: trim NEEDS ORG reminders](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_needs_org) (Katrin Leinweber)
- [Zendesk: trim org note](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_org_note) (Katrin Leinweber)
- [Zendesk: trim HIGH prio reminder](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_prio_reminder) (Katrin Leinweber)
- [Zendesk: align status & SLA](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_align_status_sla)
- [Zendesk: hide pending reminders](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_pending_reminders)
- [Zendesk: hide internal request SLA Bot bumps](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_ir_sla_bot_bumps)
- [Zendesk: OOO](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_ooo)
- [Zendesk: squeeze region & type](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_squeeze_region_type)
- [Zendesk: move Preview button](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_move_preview_button)
- [Zendesk: unsupported version](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_unsupported_version)
- [Zendesk: duplicate internal request info to CDot](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_dup_ir_to_cdot)
- [Zendesk: hide checkbox](https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_checkbox)

Other:
- [CustomersDot: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/customersdot_hayfever) deprecated
- [LicenseDot: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/licensedot_hayfever) deprecated
- [SFDC: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/sfdc_hayfever)
- [Zuora: hayfever](https://gitlab.com/rverschoor/punk/-/tree/main/zuora_hayfever)
- [slic](https://gitlab.com/rverschoor/punk/-/tree/main/slic)
- [slic: duplicate license](https://gitlab.com/rverschoor/punk/-/tree/main/slic_duplicate) deprecated

Icon credit: [Reshot](https://www.reshot.com/free-svg-icons/item/safety-pin-D2ETRQSGBF/)
