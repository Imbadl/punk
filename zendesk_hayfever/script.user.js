// ==UserScript==
// @name          Zendesk Hayfever
// @version       0.0.7
// @author        Rene Verschoor
// @description   Zendesk skin
// @match         https://gitlab.zendesk.com/*
// @grant         GM_addStyle
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hayfever
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hayfever/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hayfever/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hayfever/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const BG = 'Black';
const FG = 'LightSeaGreen';
const DIM = 'Silver';
const HOVER_BG = 'MediumAquamarine';
const HOVER_FG = 'Black';
const ACTIVE_BG = HOVER_BG;
const ACTIVE_FG = HOVER_FG;
const PRIVATE_NOTE_FG = 'Black';
const PRIVATE_NOTE_BG = 'CadetBlue';
const PUBLIC_NOTE_FG = 'Black';
const PUBLIC_NOTE_BG = 'LightCyan';

const TICKET_STATUS_NEW = '#FFB648';
const TICKET_STATUS_OPEN = '#E34F32';
const TICKET_STATUS_HOLD = '#2F3941';
const TICKET_STATUS_PENDING = '#3091EC';
const TICKET_STATUS_SOLVED = '#87929D';
const TICKET_STATUS_CLOSED = '#D8DCDE';
const TICKET_STATUS_SUSPENDED = TICKET_STATUS_HOLD;
const TICKET_STATUS_DELETED = TICKET_STATUS_HOLD;

// For debugging
const RED = 'HotPink';

let style = '';

// div => div:not(.comment)
function slam() {
  style += `
  html, body, 
  div,  
  object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, abbr, address, cite,
  code, del, dfn, em, img, ins, kbd, q, samp, small, strong, sub, sup, var, b, i, dl, dt, dd, ol, ul, li,
  fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, article, aside, canvas, details,
  figcaption, figure, footer, header, hgroup, menu, nav, section, summary, mark, audio, video {
    background-color: ${BG} !important;
    color: ${FG} !important;
  }
  `;
}

function scrollbars() {
  style += `
  /* Firefox */
  * { scrollbar-width: thin; scrollbar-color: ${FG} ${BG}; }
  /* Chrome, Edge, and Safari */
  *::-webkit-scrollbar { width: 12px; }
  *::-webkit-scrollbar-track { c }
  *::-webkit-scrollbar-thumb { background-color: ${FG}; border-radius: 20px; border: 3px solid ${BG}; }
  `;
}

function leftNavigationBar() {
  style += `
  nav#main_navigation div.product_icon > svg { color: ${FG} !important; fill: ${FG} !important; }
  nav#main_navigation { background: ${BG} !important; }
  nav#main_navigation > ul > li > div > a:not(.active-product-link) { color: ${FG} !important; }
  nav#main_navigation > ul > li > div > a.active-product-link { background: ${FG} !important; color: ${BG} !important; }
  nav#main_navigation > .apps_nav_bar > a:not(.active) > svg { fill: ${FG} !important; }
  nav#main_navigation > .apps_nav_bar > a.active { background: ${FG} !important; }
  nav#main_navigation > .apps_nav_bar > a.active > svg { background: ${FG} !important; fill: ${BG} !important; }
  `;
}

function topBar() {
  style += `
  /* This element will be covered by other elements, but peeps through below user options */
  #branding_header { background: ${BG} !important; }

  /* +Add button */
  [data-test-id="header-toolbar"] > div > div > div > button { background: ${BG} !important; color: ${FG} !important; }

  /* Bottom border */
  [data-test-id="header-toolbar"] { border-bottom-color: ${BG} !important; }

  /* Ticket tabs */
  [data-test-id="header-tab"][data-selected="true"] { background: ${FG} !important; color: ${BG} !important; border-color: ${FG} !important; }
  [data-test-id="header-tab"][data-selected="true"] > div:nth-child(1) { background: ${FG} !important; }
  [data-test-id="header-tab"][data-selected="true"] > div:nth-child(2) { background: ${FG} !important; color: ${BG} !important; }
  [data-test-id="header-tab"][data-selected="true"] > div:nth-child(2) > div { background: ${FG} !important; color: ${BG} !important; }
  [data-test-id="header-tab"][data-selected="true"] > div:nth-child(2) > div > [data-test-id="header-tab-title"] { background: ${FG} !important; color: ${BG} !important; }
  [data-test-id="header-tab"][data-selected="true"] > div:nth-child(2) > div > [data-test-id="header-tab-subtitle"] { background: ${FG} !important; color: ${BG} !important; }
  [data-test-id="header-tab"][data-selected="true"] > div:nth-child(2) > div > [data-test-id="header-tab-subtitle"] > span { background: ${FG} !important; color: ${BG} !important; }

  [data-test-id="header-tab"][data-selected="false"] { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }
  
  /* Close icon */
  [data-test-id="header-tab"][data-selected="false"] > [data-test-id="close-button"] > svg { color: ${FG} !important; }
  `
}

function viewsPanel() {
  style += `
  /* Hover view title */
  [data-test-id="views_views-list_row"]:hover { background: ${HOVER_BG} !important; color: ${HOVER_FG} !important; }
  [data-test-id="views_views-list_row"]:hover div { background: ${HOVER_BG} !important; color: ${HOVER_FG} !important; }
  /* Active view title TODO: get rid of static class */
  .fmRcNC { background: ${ACTIVE_BG} !important; color: ${ACTIVE_FG} !important; }
  .fmRcNC div { background: ${ACTIVE_BG} !important; color: ${ACTIVE_FG} !important; }

  /* Reload and collapse icons */
  [data-garden-id="buttons.icon"] { color: ${FG} !important; }

  /* Borders */
  .filters > div > div > div > div { border-right-color: ${FG} !important; }
  [data-test-id="views_views-list_header"] > div { border-bottom-color: ${FG} !important; }
  [data-test-id="views_views-list_personal-views-header"] > div { border-bottom-color: ${FG} !important; }
  [data-test-id="views_views-list_footer"] { border-top-color: ${FG} !important; }
  `;
}

function viewsMain() {
  style += `
  /* Play button */
  [data-test-id="views_views-header-play-button"] { color: ${FG} !important; border-color: ${FG} !important; }
  `;
}

function ticketsList() {
  style += `
  /* Header */
  [data-test-id="views_views-header-counter"] { background: ${BG} !important; color: ${FG} !important; z-index: 1 !important; }
  [data-test-id="views_views-header-options-button"] { color: ${BG} !important; border-color: ${FG} !important; }

  /* Table header */
  [data-test-id="table_header"] th { background: ${BG} !important; color: ${FG} !important; }
  [data-test-id="table_header"] th { border-color: ${FG} !important; }

  /* Ticket status badge */
  [data-test-id="status-badge-new"] { background: ${TICKET_STATUS_NEW} !important; color: ${BG} !important; }
  [data-test-id="status-badge-open"] { background: ${TICKET_STATUS_OPEN} !important; color: ${BG} !important; }
  [data-test-id="status-badge-hold"] { background: ${TICKET_STATUS_HOLD} !important; color: ${BG} !important; }
  [data-test-id="status-badge-pending"] { background: ${TICKET_STATUS_PENDING} !important; color: ${BG} !important; }
  [data-test-id="status-badge-solved"] { background: ${TICKET_STATUS_SOLVED} !important; color: ${BG} !important; }
  [data-test-id="status-badge-closed"] { background: ${TICKET_STATUS_CLOSED} !important; color: ${BG} !important; }
  [data-test-id="status-badge-suspended"] { background: ${TICKET_STATUS_SUSPENDED} !important; color: ${BG} !important; }
  [data-test-id="status-badge-deleted"] { background: ${TICKET_STATUS_DELETED} !important; color: ${BG} !important; }
  
  /* Rows */
  /* All td except subject */
  tr[data-test-id="table-regular-row"]:not(:hover) > td { color: ${FG} !important; }
  /* Subject */
  tr[data-test-id="table-regular-row"]:not(:hover) > td > a > span { color: ${FG} !important; }

  tr[data-test-id="table-regular-row"]:not(:hover) td { border-color: ${BG} !important; }

  /* Ticket row hover */
  /* All td except subject */
  tr[data-test-id="table-regular-row"]:hover > td { background: ${HOVER_BG} !important; color: ${HOVER_FG}; !important; }
  /* Subject */
  tr[data-test-id="table-regular-row"]:hover > td > a > span { background: ${HOVER_BG} !important; color: ${HOVER_FG}; !important; }
  
  tr[data-test-id="table-regular-row"]:hover td { border-color: ${BG} !important; }

  /* Separator bars 'Status:', 'Ticket Stage:' etc */
  tbody > tr:not([data-test-id="table-regular-row"]):not(:hover) > td { border-color: ${FG} !important; }
  tbody > tr:not([data-test-id="table-regular-row"]):not(:hover) > td { background: ${BG} !important; color: ${FG} !important; }
  tbody > tr:not([data-test-id="table-regular-row"]):hover > td { background: ${BG} !important; color: ${FG}; !important; }
  tbody > tr:not([data-test-id="table-regular-row"]):hover > td { border-color: ${FG} !important; }

  /* Indicator last opened ticket */
  tr[data-focused="true"] > td > div { border-left-color: ${FG} !important; border-left-style: dotted !important; }

  /* Paging */
  tfoot > tr > td > div > ul > li { color: ${FG} !important; border-color: ${FG} !important; }
  
  /* Collission hover (eye icon) */
  [data-test-id="table-regular-row"]:not(:hover) [data-test-id="collision-cell"] { background: ${HOVER_FG} !important; color: ${HOVER_BG} !important; } 
  [data-test-id="table-regular-row"]:hover [data-test-id="collision-cell"] { background: ${HOVER_BG} !important; color: ${HOVER_FG} !important; } 
  [data-test-id="table-regular-row"]:hover > td:nth-child(3) > div > div { background: ${HOVER_BG} !important; } 
  
  /* Hide checkbox */
  [data-test-id="table_header"] > thead > tr > th:nth-child(2) > input { visibility: hidden }
  [data-test-id="table-regular-row"] > td:nth-child(2) > input { visibility: hidden }

  /* Paging current page */
  tfoot > tr > td > div > ul > li.LRex { background: ${FG} !important; color: ${BG} !important; }
  `;
}

function ticketPanel() {
  style += `
  /* Top buttons */
  [data-test-id="customer-context-tab-customer"] { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }
  [data-test-id="customer-context-tab-ticket"] { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }

  /* Cc, tags */
  .zd-tag-menu-root { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }
  .zd-tag-item { background: ${BG} !important; }
  .zd-tag-item { border-color: ${FG} !important; }
  .zd-tag-item a { color: ${FG} !important; }

  /* Form fields */
  [data-garden-id="forms.field"] input { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }
  [data-garden-id="dropdowns.faux_input"] { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }
  [data-garden-id="forms.textarea"] { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }

  /* Borders */
  .property_box { border-color: ${FG} !important; }
  `;
}

function ticketView() {
  style += `
  /* Customer info */
  [data-test-id="tabs-nav-item-organizations"] { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important;}
  [data-test-id="tabs-nav-item-users"] { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important;}
  [data-test-id="tabs-section-nav-item-ticket"] { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important;}

  /* Side conversations */
  [data-test-id="sc-menu-button-threads"] { color: ${DIM} !important; filter: brightness(50%); }
  [data-side-conversations-toolbar-anchor-id] { border-color: ${FG} !important; }
  [data-side-conversations-toolbar-anchor-id] > div > div:nth-child(2) { background: ${FG} !important; }

  /* Apps button */
  [data-test-id="customer-context-tab-apps"] { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }

  /* ZD Support icon left of ticket title */
  .thumbnail-container.profile.ticket { background: ${BG} !important; border-color: ${FG} !important; }
  div.channel { background-color: transparent !important; }
  
  /* Commenter */
  span.name > a { background: ${BG} !important; color: ${DIM} !important; }

  /* Preview button */
  .markdown_preview_toggle { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }
  .markdown_preview_toggle.active { background: ${FG} !important; color: ${BG} !important; }

  /* Public reply/Internal note buttons */
  [data-test-id="public-reply-button"]:not(.active) { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }
  [data-test-id="public-reply-button"].active { background: ${FG} !important; color: ${BG} !important; }
  [data-test-id="private-note-button"]:not(.active) { background: ${BG} !important; color: ${FG} !important; border-color: ${FG} !important; }
  [data-test-id="private-note-button"].active { background: ${FG} !important; color: ${BG} !important; }

  /* New public reply */
  .comment_input.is-public textarea { background: ${PUBLIC_NOTE_BG} !important; color: ${PUBLIC_NOTE_FG} !important; }

  /* New private note */
  .comment_input:not(.is-public) textarea { background: ${PRIVATE_NOTE_BG} !important; color: ${PRIVATE_NOTE_FG} !important; }

  /* Private note */
  .event:not(.is-public) .comment { background: ${PRIVATE_NOTE_BG} !important; color: ${PRIVATE_NOTE_FG} !important; }
  .event:not(.is-public) .zd-comment { background: ${PRIVATE_NOTE_BG} !important; color: ${PRIVATE_NOTE_FG} !important; }
  .event:not(.is-public) .zd-comment > p { background: ${PRIVATE_NOTE_BG} !important; color: ${PRIVATE_NOTE_FG} !important; }
  
  /* Inline code block */
  div.zd-comment > code { background: ${FG} !important; color: ${BG} !important; }

  /* Dropdown next to ticket title (merge into another ticket etc.) */
  .object_options_btn { background: ${FG} !important; border-color: ${BG} !important; }

  /* Next ticket button */
  button.next_option { background: ${BG} !important; border-color: ${FG} !important; }

  /* Submit button */
  [data-test-id="submit_button-button"] { color: ${FG} !important;  }
  [data-test-id="submit_button-menu-button"] { border-left-color: ${FG} !important;  }

  /* View original mail & 'Was not signed in' button */
  .dropdown-toggle { background: ${FG} !important; color: ${BG} !important; border-color: ${FG} !important; }
  .dropdown-toggle:hover { box-shadow: inset 0 2px 0 ${BG} !important; }

  /* Assign & cc me */
  .for_save {color: ${DIM} !important; }

  /* Datetime */
  .actor > time {color: ${FG} !important; }

  `;

  // TODO URLs
  // TODO still a few borders
  // TODO All/Public/Internal counters
  // TODO rating bar (eg https://gitlab.zendesk.com/agent/tickets/220654)
  // TODO Highlighted background when you've just posted an internal comment ([data-test-id="ticket-audit-comment"] .is-new)
  // TODO Submit button background color is overruled by .LRhs
  // TODO View original mail & 'Was not signed in' button, popup colors, warning icon color
  // TODO preview of private note (div.comment_markdown_preview .event:not(.is-public) .comment background)
}

(function() {

  // Rip out ZD brand styling
  let branding = document.querySelector('#color-branding');
  if (branding) {
    branding.textContent = "";
  }

  slam();
  scrollbars();
  leftNavigationBar();

  // For https://gitlab.zendesk.com/agent/filters/*
  topBar();
  viewsPanel();
  viewsMain();
  ticketsList();

  // For https://gitlab.zendesk.com/agent/tickets/*
  ticketPanel();
  ticketView();

  GM_addStyle(style);

})();
