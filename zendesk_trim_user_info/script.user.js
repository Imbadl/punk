// ==UserScript==
// @name          ZenDesk trim user info
// @version       1.0.0
// @author        Katrin Leinweber
// @description   Zendesk: hide duplicate, default content of internal User Info comment
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_user_info
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_user_info/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_user_info/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_user_info/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const DEBUG = false;
const HEAD = "User Info";
const NAME = "Name:";
const MAIL = "Email:";
const LANG = "Language: English";
const LOC = "Locale: en-US";

(function () {

  // Watch for any change on the Zendesk page
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po();
    rm_header();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  // Find user info list entries
  function po() {
    let selector = '[id^=ember] > div.comment > div > ul > li';
    let elements = document.querySelectorAll(selector);
    elements.forEach((element, index) => {
      let text = element.textContent
      if (text.startsWith(NAME) || text.startsWith(MAIL) || text === LANG || text === LOC) {
        go(elements, index);
      }
    })
  }

  // Find user info header
  function rm_header() {
    let selector = '[id^=ember] > div.comment > div > h3';
    let elements = document.querySelectorAll(selector);
    elements.forEach((element, index) => {
      if (element.textContent === HEAD) {
        go(elements, index);
      }
    })
  }

  // Remove elements
  function go(elements, index) {
    if (DEBUG) {
      elements[index].style.backgroundColor = 'hotpink';
    } else {
      elements[index].style.display = 'none';
    }
  }

})();
