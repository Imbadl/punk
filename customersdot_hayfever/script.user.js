// ==UserScript==
// @name          CustomersDot Hayfever
// @version       1.0.2
// @author        Rene Verschoor
// @description   CustomersDot skin
// @match         https://customers.gitlab.com/admin
// @match         https://customers.gitlab.com/admin/*
// @match         https://customers.gitlab.com/subscriptions
// @match         https://customers.gitlab.com/subscriptions/*
// @match         https://customers.gitlab.com/admins/sign_in
// @grant         GM_addStyle
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/customersdot_hayfever
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/customersdot_hayfever/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/customersdot_hayfever/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/customersdot_hayfever/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const BG = 'White';  // Need to specify inverse color

(function() {
  GM_addStyle(`html { filter: invert(100%) contrast(.8) hue-rotate(180deg) !important; background: ${BG} !important; }`);
})();
