// ==UserScript==
// @name          Zendesk duplicate internal request info to CDot
// @version       1.0
// @author        Rene Verschoor
// @description   Zendesk: duplicate internal request info to CDot
// @match         https://gitlab.zendesk.com/*
// @match         https://customers.gitlab.com/admin/*
// @grant         GM_registerMenuCommand
// @grant         GM_setValue
// @grant         GM_getValue
// @grant         GM_deleteValue
// @require       https://cdn.jsdelivr.net/combine/npm/@violentmonkey/dom@2,npm/@violentmonkey/ui@0.7
// @require       https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @require       https://cdn.jsdelivr.net/npm/sweetalert2@11
// @require       message.js
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_dup_ir_to_cdot
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

// https://github.com/violentmonkey/vm-shortcut
const HOTKEY_COPY = 'ctrl-i ctrl-c';
const HOTKEY_PASTE = 'ctrl-i ctrl-v';

const AUTHOR = 'GitLab Support End User';

(function () {

  function registerHotkey() {
    VM.shortcut.register(HOTKEY_COPY, () => {
      infoCopy();
    });
    VM.shortcut.register(HOTKEY_PASTE, () => {
      infoPaste();
    });
  }

  function registerMenu() {
    GM_registerMenuCommand('IR info - copy', infoCopy);
    GM_registerMenuCommand('IR info - paste', infoPaste);
  }

  async function infoCopy() {
    await GM_deleteValue('IR_Info');
    let info = {};
    let url = document.URL;
    if (url.match(/^https:\/\/gitlab.zendesk.com\/agent\/tickets\/\d+/)) {
      info = scrapeInfo(url);
      await GM_setValue('IR_Info', JSON.stringify(info));
      toast_ok('Info copied');
    } else {
      toast_error('Can only copy info from a Zendesk ticket');
    }
  }

  function scrapeInfo(url) {
    let info = {};
    const title = getTicketTitle();
    if (title === 'IR - SM - Extend an existing trial') {
      info = scrapeInfo_sm_extend_existing_trial(url);
    } else if (title === 'IR - SM - Extend an (almost) expired subscription') {
      info = scrapeInfo_sm_extend_expired_subscription(url);
    } else if (title === 'IR - SM - Wider Community License') {
      info = scrapeInfo_sm_wider_community_license(url);
    } else if (title === 'IR - SM - NFR license request') {
      info = scrapeInfo_sm_nfr_license_request(url);
    } else if (title === 'IR - SM - Hacker One Reporter License') {
      info = scrapeInfo_sm_hacker_one_license(url);
    } else {
      toast_error('Ticket type not recognized');
    }
    return info;
  }

  function getTicketTitle() {
    return document.querySelector('div [data-test-id="header-tab-title"]').textContent;
  }

  function getFirstComment() {
    const comments = document.querySelectorAll('[data-test-id="ticket-audit-comment"]');
    if (!comments?.length) {
      toast_error('Ticket has no comments');
      return null;
    }
    const comment = comments[comments.length - 1];
    const author = comment.querySelector('div.content .header .actor .name').textContent.trim();
    if (author !== AUTHOR) {
      toast_error(`Unexpected author: ${author}`);
      return null;
    }
    return comment;
  }

  function commentToList(comment) {
    let list = {};
    const nodes = comment.querySelectorAll('.zd-comment ul li');
    const texts = [...nodes].map(node => node.textContent);
    texts.forEach(text => {
      const keyval = text.split(/:(.*)/s);
      list[keyval[0].trim()] = keyval[1].trim();
    });
    return list;
  }

  function getList() {
    const comment = getFirstComment();
    const list = commentToList(comment);
    return list;
  }

  function scrapeInfo_sm_extend_existing_trial(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Company",
      "User count",
      "Plan",
      "Desired expiration date");
    values.trial = 'Yes';
    values.notes = url;
    return values;
  }

  function scrapeInfo_sm_extend_expired_subscription(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Contact's company",
      "Seats needed in temp license",
      "Plan for temp license",
      "Desired expiration date");
    values.trial = 'Yes';
    values.notes = url;
    return values;
  }

  function scrapeInfo_sm_wider_community_license(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Contact's company",
      "User count",
      "Plan",
      "Expiration date");
    values.trial = 'Yes';
    values.notes = url;
    return values;
  }

  function scrapeInfo_sm_nfr_license_request(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Contact's company",
      "User count",
      "Plan",
      "License end date");
    values.trial = 'Yes';
    values.notes = url;
    return values;
  }

  function scrapeInfo_sm_hacker_one_license(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Company",
      "Number of users",
      "Plan",
      "License duration");
    values.expiration = '';  // clear field again
    values.trial = 'Yes';
    values.notes = url;
    return values;
  }

  function scrapeTicket(list, name, email, company, userscount, plan, expiration) {
    let values = {};
    values.name = list[name];
    values.email = list[email];
    values.company = list[company];
    values.userscount = list[userscount];
    values.plan = list[plan];
    values.expiration = list[expiration];
    return values;
  }

  async function infoPaste() {
    let url = document.URL;
    if (url.match(/^https:\/\/customers.gitlab.com\/admin\/license\/new_license/)) {
      let info = JSON.parse(await GM_getValue('IR_Info', '{}'));
      infoFill(info);
      await GM_deleteValue('IR_Info');
    } else {
      toast_error('Can only duplicate to CustomersDot > New License');
    }
  }

  function infoFill(info) {
    setField('license_name', info.name);
    setField('license_company', info.company);
    setField('license_email', info.email);
    setField('license_users_count', info.userscount);
    setPlan('license_plan_code', capitalize(info.plan));
    setCheckbox('license_trial', info.trial);
    setField('license_expires_at', info.expiration);
    setField('license_notes', info.notes);
  }

  function setField(field, value) {
    document.getElementById(field).value = value;
  }

  function setPlan(field, plan) {
    document.querySelector('[data-input-for="license_plan_code"] .input-group-btn').click();
    [...document.querySelector('ul#ui-id-2').children].filter(option => option.textContent === plan)[0].click();
  }

  function setCheckbox(field, text) {
    let value = text === 'Yes' ? true : false;
    let checkbox = document.getElementById(field);
    if (checkbox.checked != value) {
      checkbox.click();
    }
  }

  function capitalize(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  registerMenu();
  registerHotkey();
  // console.log('@@@ 056');  // canary

})();
