# Zendesk duplicate internal request info to CDot

## Purpose

A userscript for Zendesk tickets.

This script copies information from an Internal Request to the CDot `New License` form. \
Supported internal requests:
- `IR - SM - Extend an existing trial`
- `IR - SM - Extend an (almost) expired subscription`
- `IR - SM - Hacker One Reporter License`
- `IR - SM - Wider Community License`
- `IR - SM - NFR license request`

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js

## Use

Hotkeys:
- `ctrl-i-c` to copy info from the Zendesk IR ticket
- `ctrl-i-v` to paste info into CDot's `New License` form

Alternatively use the commands from the userscript manager extension dropdown:
- `Zendesk duplicate internal request info to CDot > IR info - copy` 
- `Zendesk duplicate internal request info to CDot > IR info - paste`

License duplication is a two step manual process:
- Go to the Zendesk IR ticket to copy information
- Go to the `New License` form in CDot to paste the information

The script ignores any `true-ups` entered in the IR, and doesn't fill in PUC and TUC in CDot. \
You will have to do the unholy math yourself.

Always double check the pasted information!

## Changelog

- 1.0.0
  - Public release
