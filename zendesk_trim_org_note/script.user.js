// ==UserScript==
// @name          ZenDesk trim org notes
// @version       1.0.0
// @author        Katrin Leinweber
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_org_note
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_org_note/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_org_note/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_org_note/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/-/issues/new
// ==/UserScript==

'use strict';

const DEBUG = false;
const ORG = "Organization Notes:";
const CC = "NOTE: This user requested the following people be added as CCs on the ticket:";
const REGION = "Preferred Region:";
const LEVEL = " Support Level: Premium:";
const ACCOUNT = "Account type:";
const SEGMENT = "Sales Segmentation:";
const AO = "Account Owner:";
const TAM = "Technical Account Manager:";
const UPGRADE = "Manual Support Upgrade:";


(function () {

  // Watch for any change on the Zendesk page
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  // Find note content
  function po() {
    let selector = '[id^=ember] > div.comment > div > p';
    let elements = document.querySelectorAll(selector);
    elements.forEach((element, index) => {
      let text = element.textContent
      if (text.startsWith(CC) || text.startsWith(ORG) || text.startsWith(AO)) {
        // || text.startsWith(REGION)
        // || text.startsWith(LEVEL)
        // || text.startsWith(ACCOUNT)
        // || text.startsWith(SEGMENT)
        // || text.startsWith(TAM)
        // || text.startsWith(UPGRADE)
        // only 2 <p> blocks
        go(elements, index);
      }
    })
  }


  // Remove elements
  function go(elements, index) {
    if (DEBUG) {
      elements[index].style.backgroundColor = 'hotpink';
    } else {
      elements[index].style.display = 'none';
    }
  }

})();
