# Zendesk trim org note

## Purpose

A userscript for Zendesk tickets.

This script removes entries from org notes that were duplicated from the sidebar. 
This compacts the ticket, making it a bit more readable, and have you scroll less.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_org_note/script.user.js

## Technical

The script uses a `MutationObserver` to watch for changes on the Zendesk page. Normally you would hook to e.g. the `DOMContentLoaded` event, but I couldn't get that working with Zendesk. The script was not triggered when another ticket was loaded in a tab. My guess is that this is caused by Zendesk building dynamic pages using Ember. Using `MutationObserver` solved it, at the cost of unneeded triggers of the script.

## Changelog

- 1.0.0
  - Public release
