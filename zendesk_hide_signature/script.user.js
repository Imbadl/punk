// ==UserScript==
// @name          Zendesk hide signature
// @version       1.0.2
// @author        Rene Verschoor
// @description   Zendesk: hide agent signature and feedback line in all comments
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_signature
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_signature/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_signature/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_signature/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const DEBUG = false;
const FEEDBACK = "Want to submit feedback on this ticket so far? Click here to tell us how we're doing!";

(function() {

  // Watch for any change on the Zendesk page
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  // Find the feedback text
  function po() {
    let selector = '[id^=ember] > div.comment > div > p';
    let elements = document.querySelectorAll(selector);
    elements.forEach( (element, index) => {
      if (element.textContent === FEEDBACK) {
        go(elements, index);
      }
    })
  }

  // Remove elements
  function go(elements, index) {
    if (DEBUG) {
      elements[index].style.backgroundColor = 'hotpink';
      elements[index-1].style.backgroundColor = 'hotpink';
    } else {
      elements[index].style.display = 'none';
      elements[index-1].style.display = 'none';
    }
  }

})();