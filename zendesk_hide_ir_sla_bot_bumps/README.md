# Zendesk hide internal request SLA Bot bumps

## Purpose

A userscript for Zendesk tickets.

This script hides the automatic SLA Bot bump messages resulting from [Have controlled end-user reply to get SLA timer back on L&R internal requests (#711) · Issues · GitLab.com / GitLab Support Team / Support Operations / Support Ops Project · GitLab](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project/-/issues/711). \
This compacts the ticket, making it a bit more readable, and makes you scroll less.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_ir_sla_bot_bumps/script.user.js

## Changelog

- 1.0.0
  - Public release
