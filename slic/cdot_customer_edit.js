'use strict';

function scrapeCDotCustomerEdit(url) {
  let info = {};
  info.url = url;

  info['first_name'] = document.querySelector('#customer_first_name_field input').value;
  info['last_name'] = document.querySelector('#customer_last_name_field input').value;
  info['email'] = document.querySelector('#customer_email_field input').value;
  info['zuora_account'] = document.querySelector('#customer_zuora_account_id_field input').value;
  info['sfdc_account'] = document.querySelector('#customer_salesforce_account_id_field input').value;
  info['login_activated'] = document.querySelector('#customer_login_activated').checked;
  return pasteCDotCustomerEdit(info);
}

function pasteCDotCustomerEdit(info) {
  let text =
    `URL = ${info.url}\n` +
    `First name = ${info.first_name}\n` +
    `Last name = ${info.last_name}\n` +
    `Email = ${info.email}\n` +
    `Zuora account = ${info.zuora_account}\n` +
    `SFDC account = ${info.sfdc_account}\n` +
    `Login activated = ${info.login_activated}\n`
  ;
  return text;
}

